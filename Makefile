###############################################################################
#
# Makefile 
#
###############################################################################

##
## V A R I A B L E S
##

# This Makefile has many input variables, 
# see link below for the standard vars offered by PGXS
# https://www.postgresql.org/docs/current/extend-pgxs.html

# The input variable below are optional

# PSQL : psql client ( default = local psql )
# PGDUMP : pg_dump tool  ( default = docker )
# PG_TEST_EXTRA : extra tests to be run by `installcheck` ( default = none )

##
## C O N F I G
##

EXTENSION = dump_table_schema 
EXTENSION_VERSION=$(shell grep default_version $(EXTENSION).control | sed -e "s/default_version[[:space:]]*=[[:space:]]*'\([^']*\)'/\1/")
#DATA = data/*
# Use this var to add more tests
REGRESS = basic 
REGRESS+=$(PG_TEST_EXTRA)
MODULEDIR=extension/dump_table_schema
REGRESS_OPTS = --inputdir=tests

##
## B U I L D
##

.PHONY: extension
extension: 
	mkdir -p dump_table_schema
	cp dump_table_schema.sql dump_table_schema/dump_table_schema--$(EXTENSION_VERSION).sql

PG_DUMP?=docker exec postgresqlanonymizer_PostgreSQL_1 pg_dump -U postgres --insert --no-owner 
PSQL?=PGPASSWORD=CHANGEME psql -U postgres -h 0.0.0.0 -p54322
PGRGRSS=docker exec postgresqlanonymizer_PostgreSQL_1 /usr/lib/postgresql/10/lib/pgxs/src/test/regress/pg_regress --outputdir=tests/ --inputdir=./ --bindir='/usr/lib/postgresql/10/bin'  --inputdir=tests --dbname=contrib_regression --user=postgres unit

##
## D O C K E R
##

docker_image: Dockerfile
	docker build -t registry.gitlab.com/daamien/postgresql_anonymizer .

docker_push:
	docker push registry.gitlab.com/daamien/postgresql_anonymizer

docker_bash:
	docker exec -it postgresqlanonymizer_PostgreSQL_1 bash

COMPOSE=docker-compose

docker_init:
	$(COMPOSE) down
	$(COMPOSE) up -d
	@echo "The Postgres server may take a few seconds to start. Please wait."


.PHONY: expected
expected : tests/expected/unit.out

tests/expected/unit.out:
	$(PGRGRSS)
	cp tests/results/unit.out tests/expected/unit.out

##
## D E M O   &   T E S T S
##

demo:
	psql -f tests/sql/basic.sql
	pg_dump -x -s -t basic > /tmp/basic.original.out
	psql -qAtX -c "SELECT dump_table_schema('basic')" > /tmp/basic.reload.sql
	psql -qAtX -c "DROP TABLE basic"	
	psql -f /tmp/basic.reload.sql
	pg_dump -x -s postgres -t basic > /tmp/basic.reloaded.out	
	diff /tmp/basic.original.out /tmp/basic.reloaded.out

##
## C I
##

.PHONY: ci_local
ci_local:
	gitlab-ci-multi-runner exec docker make

##
## P G X N
##

ZIPBALL:=$(EXTENSION)-$(EXTENSION_VERSION).zip

.PHONY: pgxn

$(ZIPBALL): pgxn

pgxn:
	# required by CI : https://gitlab.com/gitlab-com/support-forum/issues/1351
	git clone --bare https://gitlab.com/daamien/dump_table_schema.git
	git -C dump_table_schema.git archive --format zip --prefix=$(EXTENSION)_$(EXTENSION_VERSION)/ --output ../$(ZIPBALL) master
	# open the package	
	unzip $(ZIPBALL)
	# remove the zipball because we will rebuild it from scratch
	rm -fr $(ZIPBALL)
	# copy artefact into the package
	cp -pr anon ./$(EXTENSION)_$(EXTENSION_VERSION)/
	# remove files that are useless in the PGXN package 
	rm ./$(EXTENSION)_$(EXTENSION_VERSION)/*.gif ./$(EXTENSION)_$(EXTENSION_VERSION)/Dockerfile*
	# rebuild the package
	zip -r $(ZIPBALL) ./$(EXTENSION)_$(EXTENSION_VERSION)/
	# clean up
	rm -fr ./$(EXTENSION)_$(EXTENSION_VERSION) ./dump_table_schema.git/


##
## Mandatory PGXS stuff
##
PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
