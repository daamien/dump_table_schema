# dump_table_schema

This is a pure SQL implementation to export the DDL structure of a table.

## Other Implementations

* [pg_dump (C)](https://github.com/postgres/postgres/blob/master/src/bin/pg_dump/pg_dump.c#L15466) is supposed to be a "correct" implementation
* [TablePlus (JS)](https://github.com/TablePlus/tabledump)
* [pgadmin (Python)](https://github.com/postgres/pgadmin4/blob/master/web/pgadmin/browser/server_groups/servers/databases/schemas/tables/utils.py#L858)

## Links 

* [MySQL : SHOW CREATE TABLE](https://dev.mysql.com/doc/refman/8.0/en/show-create-table.html)
* [Stackoverflow : How to generate the “create table” sql statement for an existing table in postgreSQL](https://stackoverflow.com/questions/2593803/how-to-generate-the-create-table-sql-statement-for-an-existing-table-in-postgr)
